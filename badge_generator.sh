#!/bin/sh
      
#title           :badge_generator.sh
#description     :Conference attendees badges generator script (with Vcard QRCODE) 
#author	     	 :Ibrahim Gharbi
#date            :201808
#version         :0.1    
#usage		     : ./badge_generator.sh  attendees.csv badge.png +700-1650 800x800 0,-650 didactgothic-regular.ttf
#mail            :brah.gharbi@gmail.com
#twitter         :@__brah
#github          :https://github.com/ibrah
#==============================================================================

CSV_FILENAME="data.csv"
BADGE_TEMPLATE_FILENAME="badge_template.jpg"

QRCODE_OFFSET_X_Y=-300+670 #FORMAT: +XXX-YYY
QRCODE_SIZE="250x250"       #FORMAT: 800x800

TEXT1_OFFSET_X_Y=+00-100
TEXT1_FONT_FILENAME="fonts/StagSans-Medium.otf"

TEXT2_OFFSET_X_Y=+00+50
TEXT2_FONT_FILENAME="fonts/StagSans-Medium.otf"

TEXT3_OFFSET_X_Y=+00+200
TEXT3_FONT_FILENAME="fonts/StagSans-Light.otf"

TEXT4_OFFSET_X_Y=+00+350
TEXT4_FONT_FILENAME="fonts/StagSans-Light.otf"

TEXT_SIZE=130
TEXT_COLOR="black"

counter=1
while IFS=";" read f1 f2 f3 f4 f5 f6 f7 f8
do
        if [ $counter != 1 ]
        then
                # retrieve QRCODE 
                url="https://chart.googleapis.com/chart?chld=L|0&chs=255x255&cht=qr&chl=BEGIN%3AVCARD%0AN%3A${f7}%20${f8}%0ATEL%3A${f5}%0AEMAIL%3A${f6}%0AEND%3AVCARD"
                wget -O out/temp_qrcode.png $url
        
                # insert it in badge frame with ImageMagick 
                convert -resize $QRCODE_SIZE out/temp_qrcode.png out/temp_qrcode.png
                composite -gravity center  -quality 100 -geometry $QRCODE_OFFSET_X_Y out/temp_qrcode.png $BADGE_TEMPLATE_FILENAME out/result.png

                # insert the firsname of a participant
                convert -font $TEXT1_FONT_FILENAME -fill $TEXT_COLOR -pointsize $TEXT_SIZE -gravity center  -quality 100  -draw "text $TEXT1_OFFSET_X_Y '${f1}'" out/result.png out/result_withtext1_${counter}.png
                # insert the lastname of a participant
                convert -font $TEXT2_FONT_FILENAME -fill $TEXT_COLOR -pointsize $TEXT_SIZE -gravity center  -quality 100  -draw "text $TEXT2_OFFSET_X_Y '${f2}'" out/result_withtext1_${counter}.png out/result_withtext2_${counter}.png
                # insert the company of a participant
                convert -font $TEXT3_FONT_FILENAME -fill $TEXT_COLOR -pointsize $TEXT_SIZE -gravity center  -quality 100  -draw "text $TEXT3_OFFSET_X_Y '${f3}'" out/result_withtext2_${counter}.png out/result_withtext3_${counter}.png
                # insert the country of a participant
                convert -font $TEXT4_FONT_FILENAME -fill $TEXT_COLOR -pointsize $TEXT_SIZE -gravity center  -quality 100  -draw "text $TEXT4_OFFSET_X_Y '${f4}'" out/result_withtext3_${counter}.png out/result_withtext4_${counter}.png

                rm -r out/result.png
                rm -r out/temp_qrcode.png
                rm -r out/result_withtext1_${counter}.png
                rm -r out/result_withtext2_${counter}.png
                rm -r out/result_withtext3_${counter}.png

        fi
        counter=`expr $counter + 1`

done < $CSV_FILENAME

